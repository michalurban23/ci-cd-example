package com.codecool.michalurban.cicdexample.ping;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/ping")
public class PingController {

    PingRepositoryMock repository;

    public PingController(PingRepositoryMock repository) {

        this.repository = repository;
    }

    @GetMapping(path = "")
    public Iterable<Ping> index() {

        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Ping show(@PathVariable Integer id) {

        return repository.show(id);
    }
}
