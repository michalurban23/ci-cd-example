package com.codecool.michalurban.cicdexample.ping;

import org.springframework.stereotype.Repository;

@Repository
public class PingRepositoryMock {

    public Iterable<Ping> findAll() {

        return null;
    }

    public Ping show(Integer id) {

        return new Ping();
    }
}
